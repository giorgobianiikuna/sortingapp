package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppTest {

    @Parameterized.Parameters
    public static Collection<Object[]> testCases() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, new int[]{}},
                {new String[]{"3"}, new int[]{3}},
                {new String[]{"1", "2", "3", "5", "4", "6", "9", "8", "10", "7"}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new String[]{"1", "2", "3", "5", "4", "6", "9", "8", "10", "7", "12", "11", "13"}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10 , 11, 12, 13}}
        });
    }
    private String[] input;
    private int[] expected;

    public SortingAppTest(String[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void testSortingApp() {
        SortingApp.main(input);

        int[] actualOutput = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            actualOutput[i] = Integer.parseInt(input[i]);
        }

        assertArrayEquals(expected, actualOutput);
    }
}
