package com.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class SortingApp {

    private static final Logger logger = LogManager.getLogger(SortingApp.class);
    public static void main(String[] args) {
        if (args.length == 0) {
            logger.warn("The number of arguments provided was 0.");
            return;
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }

        Arrays.sort(numbers);

        for (int i = 0; i < numbers.length; i++) {
            args[i] = Integer.toString(numbers[i]);
        }

        System.out.println("Sorted numbers:");
        for (int number : numbers) {
            System.out.println(number);
        }
    }
}
